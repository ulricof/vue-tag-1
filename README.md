# vue-intro

## Bootstrap einbinden

1. Im Projektordner `npm install bootstrap`
2. In der `App.vue` zweiten `style`-Block mit dem Pfad zur heruntergeladenen Bootstrap-Datei einbinden

**ACHTUNG:** es ist (vorerst) nicht möglich, das Bootstrap-JS (bspw. Dialoge, etc...) bei Vue zu verwenden. Dazu sind
noch einige Anpassungen notwendig, die wir erst in den nächsten Theorieeinheiten besprechen werden.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
